import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// Components
import { UsersComponent } from './components/users/users.component';
import { NewuserComponent } from './components/newuser/newuser.component';

// Routes
const routes: Routes = [
	{
		path : 'users',
		component: UsersComponent,
	},
	{
		path : 'newusers',
		component: NewuserComponent,
	},
	{
		path : '**',
		pathMatch : 'full',
		redirectTo: 'users'
 	}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
