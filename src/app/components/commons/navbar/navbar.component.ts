import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  private activeRoute: string = 'users';

  constructor(
  	private router: Router,
  	) {
  		router.events.subscribe((val:any) => {
  			if (val.url) {
	  			if ((val.url === '/') || (val.url === '/users')) {
		  			this.activeRoute = 'users';
		  		} else {
		  			this.activeRoute = 'newusers';
		  		}
		  	}
  		});
  	}

  ngOnInit() {
  }



}
